<?php
/**
 * Created by PhpStorm.
 * User: Ibrael Espinosa com-i.espinosa@lagardere-active.com
 * Date: 04/03/14
 * Time: 15:41
 *
 * Simple autoloader that follow the PHP Standards Recommendation #0 (PSR-0)
 * @see https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-0.md for more informations.
 *
 * Code inspired from the SplClassLoader RFC
 * @see https://wiki.php.net/rfc/splclassloader#example_implementation
 */

$namespaces = array(
    'La\\Lib\\Messaging\\Test' => "../tests/",
    'La\\Lib\\Messaging' => "La/Lib/Messaging",
);

spl_autoload_register(
    function ($className) use ($namespaces) {

        $className = ltrim($className, '\\');

        if ($lastNsPos = strripos($className, '\\')) {
            $namespace = substr($className, 0, $lastNsPos);
            $className = substr($className, $lastNsPos + 1);

            foreach ($namespaces as $nm => $dir) {
                if (substr($namespace, 0, strlen($nm)) == $nm) {
                    $namespace = $dir . str_replace($nm, "", $namespace);
                }
            }

            $fileName = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
            $fileName = __DIR__ . DIRECTORY_SEPARATOR . $fileName . $className . '.php';

            if (file_exists($fileName)) {
                require $fileName;
                return true;
            }
        }

        return false;
    }
);