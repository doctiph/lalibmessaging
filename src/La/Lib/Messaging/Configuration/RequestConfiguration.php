<?php
/**
 * Created by PhpStorm.
 * User: Ibrael Espinosa com-i.espinosa@lagardere-active.com
 * Date: 24/03/14
 * Time: 15:57
 */

namespace La\Lib\Messaging\Configuration;


use La\Lib\Messaging\Traits\Connector;

class RequestConfiguration
{

    use Connector;

    /**
     * @param $server
     * @param $login
     * @param $pwd
     * @param $key
     */
    function __construct($server, $login, $pwd, $key)
    {
        $this->login = $login;
        $this->pwd = $pwd;
        $this->key = $key;

        $url = sprintf("http://%s/apitransactional/services/TransactionalService?wsdl", $server);
        $this->soapClient = new \SoapClient($url);
    }

    /**
     * Gets the templates configured into smartfocus in array format compatible with
     * mailer configuration format
     *
     * @param $type
     * @return array
     * @throws \Exception
     */
    public function getConfiguredTemplates($type)
    {
        if (!in_array($type, array('TRANSACTIONAL', 'SOCIALNETWORK'))) {
            throw new \Exception(sprintf("The %s type is not defined into smartfocus api", $type));
        }

        $templates = array();

        $result = $this->soapClient->getLastTemplates(
            array(
                'token' => $this->token,
                'type' => $type,
                'limit' => 100
            )
        );

        foreach ($result->return as $tempId) {
            $tpl = $this->soapClient->getTemplate(
                array(
                    'token' => $this->token,
                    'id' => $tempId,
                )
            )->return;

            $vars = array();
            $tempArray = (array)$tpl;

            // searching variables into all template properties
            foreach ($tempArray as $property => $value) {
                preg_match_all("/\[EMV DYN\](?P<vars>\w+)\[EMV \/DYN]/", $tpl->$property, $matches);
                if (isset($matches['vars'])) {

                    // avoiding repeated variables
                    foreach ($matches['vars'] as $variableName) {
                        if (!in_array($variableName, $vars)) {
                            $vars[] = $variableName;
                        }
                    }
                }
            }

            $templates[str_replace(' ', '_', $tpl->name)] = array(
                'random' => $tpl->random,
                'encrypt' => $tpl->encrypt,
                'dyn' => $vars
            );
        }

        return $templates;
    }
} 