<?php
/**
 * Created by PhpStorm.
 * User: Ibrael Espinosa com-i.espinosa@lagardere-active.com
 * Date: 20/03/14
 * Time: 16:07
 */

namespace La\Lib\Messaging\Mailer;

use La\Lib\Messaging\Traits\MailerApi;

/**
 * Fgets client for smartfocus api. Provides an
 * alternative when curl or soap are not availables as php extensions
 *
 * Class SmartFocusFGetMailer
 * @package La\Lib\Messaging\Mailer
 */
class SmartFocusFGetMailer extends Mailer
{

    use MailerApi;
    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function send(array $messages, &$failures = array())
    {
        $failures = (array)$failures;

        // Defines the NMP API URL
        $_API_URL = $this->getOptions()['url'];

        $xml = $this->getBatchXml($messages);

        // Push the xml feed to NMP API URL
        if ($xml) {
            $http_response = '';

            // Open socket connection and push data
            $fp = fsockopen("$_API_URL", 80);
            fputs($fp, "POST /NMSXML HTTP/1.1\r\n");
            fputs($fp, "Content-Type: text/xml\r\n");
            fputs($fp, "Connection: Keep-alive\r\n");
            fputs($fp, "Content-length: " . strlen($xml) . "\r\n");
            fputs($fp, "Host: $_API_URL\r\n\r\n");
            fputs($fp, $xml);

            // Get response
            while (!feof($fp)) {
                $http_response .= fgets($fp, 128);
            }

            // Close socket connection
            fclose($fp);

            $response = (array)simplexml_load_string($this->parse_http_response($http_response)['body']);
            $this->extractErrorsFromResponse($response, $failures);
        }

        return count($messages) - count($this->failures);
    }


    /**
     * Parse http response
     * @param $string
     * @return array
     */
    private function parse_http_response($string)
    {

        $headers = array();
        $content = '';
        $str = strtok($string, "\n");
        $h = null;
        while ($str !== false) {
            if ($h and trim($str) === '') {
                $h = false;
                continue;
            }
            if ($h !== false and false !== strpos($str, ':')) {
                $h = true;
                list($headername, $headervalue) = explode(':', trim($str), 2);
                $headername = strtolower($headername);
                $headervalue = ltrim($headervalue);
                if (isset($headers[$headername])) {
                    $headers[$headername] .= ',' . $headervalue;
                } else {
                    $headers[$headername] = $headervalue;
                }
            }
            if ($h === false) {
                $content .= $str . "\n";
            }
            $str = strtok("\n");
        }
        return array('headers' => $headers, 'body' => trim($content));
    }


}