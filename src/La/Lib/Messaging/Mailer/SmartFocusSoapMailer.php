<?php
/**
 * Created by PhpStorm.
 * User: Ibrael Espinosa com-i.espinosa@lagardere-active.com
 * Date: 18/03/14
 * Time: 14:50
 */

namespace La\Lib\Messaging\Mailer;


use La\Lib\Messaging\Message\SmartFocusMessage;
use La\Lib\Messaging\Template\SmartFocusTemplate;
use La\Lib\Messaging\Traits\MailerApi;

class SmartFocusSoapMailer extends Mailer
{
    use MailerApi;

    /**
     * @var \SoapClient
     */
    protected $client;


    /**
     * @var SmartFocusTemplate
     */
    protected $template;

    /**
     * @var array
     */
    protected $messages = array();

    /**
     *
     * @param array $config
     */
    public function __construct(array $config)
    {
        $config = array_merge(array(
            'url' => 'http://api.notificationmessaging.com/'
        ), $config);

        $this->setOptions($config);

        $url = sprintf("%sNMSOAP/NotificationService?wsdl", $this->getOptions()['url']);
        if (!@file_get_contents($url)) {
            $this->client = null;
        } else {
            $this->client = new \SoapClient($url);
        }
        $this->failures = array();
    }


    /**
     * @return array<SmartFocusMessage>
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function send(array $messages, &$failures = null)
    {
        $failures = (array)$failures;

        $this->messages = $messages;

        $sendRequests = array();

        foreach ($messages as $message) {
            $sendRequests[] = $this->createSendRequestObject($message, uniqid());
        }

        $args = array(
            'arg0' => array(
                'sendrequest' => $sendRequests
            )
        );
        if(is_null( $this->client)){
            throw new \Exception('Invalid Soap Client');
        }
        try {
            // soap call
            $response = $this->client->sendObjectsWithFullStatus($args);

            $this->extractErrorsFromResponse((array)$response->return, $failures);

            $this->failures = $failures;

        } catch (\SoapFault $e) {
            throw new \Exception($e->getMessage());
        }

        return count($messages) - count($failures);
    }

    /**
     * @param SmartFocusMessage $message
     * @return \stdClass
     */
    private function createSendRequestObject(SmartFocusMessage $message)
    {
        $sendRequest = new \stdClass;
        $sendRequest->email = $message->getEmail();
        $sendRequest->encrypt = $this->getTemplate()->getEncrypt();
        $sendRequest->random = $this->getTemplate()->getRandom();
        $sendRequest->senddate = $message->getSenddate()->format('c');
        $sendRequest->synchrotype = $message->getStype();
        $sendRequest->uidkey = $message->getUidkey();
        $sendRequest->notificationId = $message->getNotificationId();

        $sendRequest->content = $this->getTemplate()->getContent() != null ?
            $this->arrayToEntries(
                $this->getTemplate()->getContent()
            ) : null;


        $sendRequest->dyn = ($message->getDyn() != null && count($message->getDyn())) ? $this->arrayToEntries(
            $message->getDyn()
        ) : null;

        return $sendRequest;
    }

    /**
     * Convert array key=>val To look like Entry type sequence on emailvision wsdl
     *
     * @param $array
     *
     * @return array
     */
    private function arrayToEntries($array = array())
    {
        $entries = array();

        foreach ($array as $key => $parameter) {
            $entries[] = array('key' => $key, 'value' => $parameter);
        }

        return $entries;
    }


    /**
     * Callback function used after sending message. First params
     * passed to the array is the mailer itself.
     *
     * @param $function
     */
    public function onSuccess($function)
    {
        if (is_callable($function)) {
            $args = func_get_args();
            array_shift($args);
            array_unshift($args, $this);
            call_user_func_array($function, $args);
        }
    }

    /**
     * Callback function on error. First params passed to the array
     * is the mailer itself.
     *
     * @param $function
     */
    public function onError($function)
    {
        if (is_callable($function)) {
            $args = func_get_args();
            array_shift($args);
            array_unshift($args, $this);
            call_user_func_array($function, $args);
        }
    }
}