<?php
/**
 * Created by PhpStorm.
 * User: Ibrael Espinosa com-i.espinosa@lagardere-active.com
 * Date: 04/03/14
 * Time: 11:00
 */

namespace La\Lib\Messaging\Mailer;


use La\Lib\Messaging\Message\SmartFocusMessage;
use La\Lib\Messaging\Template\SmartFocusTemplate;
use La\Lib\Messaging\Traits\MailerApi;
use La\Lib\Messaging\Util\Curl;

class SmartFocusRestMailer extends Mailer
{

    use MailerApi;

    /**
     * @var Curl
     */
    protected $curl;


    public function __construct(array $config = array())
    {
        $config = array_merge(
            array(
                'url' => 'http://api.notificationmessaging.com/',
                'post' => false,
                'batch' => false,
            ),
            $config
        );

        parent::__construct($config);

        $this->curl = new Curl();

        $this->curl->setOpt(CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1');

        $this->curl->setHeader('X-Requested-With', 'XMLHttpRequest');
    }




    /**
     *
     * Send the message and return the amount of emails sent
     *
     * {@inheritdoc}
     * @return integer|mixed
     */
    public function send(array $messages, &$failures = array())
    {

        $this->messages = $messages;
        $this->failures = array();

        if ($this->getTemplate() == null) {
            throw new \Exception(sprintf("You must specify a template before sending emails"));
        }

        // checking all messages are instance of SmartFocusMessage
        foreach ($messages as $msg) {
            if (!($messages[0] instanceof SmartFocusMessage)) {
                throw new \LogicException(sprintf("The message must un %s object", 'SmartFocusMessage'));
            }
        }

        // if it's a batch call
        if ($this->isBatchRequest($messages)) {
            return $this->sendBatch($messages, $failures);
        }

        $url = sprintf(
            "%sNMSREST?%s&%s",
            $this->getOptions()['url'],
            $this->getTemplate()->getUrlQuery(),
            $messages[0]->getUrlQuery()
        );

        $this->curl->get($url);

        $response = (array)simplexml_load_string($this->curl->getResponse());
        $this->extractErrorsFromResponse($response, $failures);

        return count($messages) - count($this->failures);
    }


    /**
     * Determines when is batch request from email parameter
     * @param array $messages
     * @return bool
     */
    public function isBatchRequest(array $messages = array())
    {
        return count($messages) > 1;
    }

    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function sendBatch(array $messages = array(), &$failures = array())
    {
        $url = sprintf("%s/NMSXML", $this->getOptions()['url']);

        $this->curl->setOpt(CURLOPT_URL, $url);
        $this->curl->setHeader('Content-type', 'application/xml');
        $this->curl->post($url, $this->getBatchXml($messages));

        $response = (array)simplexml_load_string($this->curl->getResponse());
        $this->extractErrorsFromResponse($response, $failures);

        return count($this->messages) - count($this->failures);
    }
}