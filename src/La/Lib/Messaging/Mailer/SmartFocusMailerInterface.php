<?php
/**
 * Created by PhpStorm.
 * User: Ibrael Espinosa com-i.espinosa@lagardere-active.com
 * Date: 04/03/14
 * Time: 10:54
 */

namespace La\Lib\Messaging\Mailer;

/**
 * Interface MailerInterface
 *
 * Define the client functionalities
 *
 * @package libmessaging\Mailer
 */
interface SmartFocusMailerInterface
{

    /**
     *
     * @param array $config
     */
    public function __construct(array $config);

    /**
     * Send the email(s)
     *
     * @param array <SmartFocusMessage> $smessages
     * @return mixed
     */
    public function send(array $messages);

    /**
     * Set the template parameter
     *
     * @param $template
     * @return SmartFocusMailerInterface
     */
    public function setTemplate($template);

    /**
     * Set the options for sending email
     *
     * @param array $options
     * @return SmartFocusMailerInterface
     */
    public function setOptions(array $options);
} 