<?php

namespace La\Lib\Messaging\Mailer;

use La\Lib\Messaging\Message\SmartFocusMessage;
use La\Lib\Messaging\Template\SmartFocusTemplate;
use La\Lib\Messaging\Traits\MailerApi;

abstract class Mailer implements SmartFocusMailerInterface
{

    use MailerApi;
    /**
     * @var array $options
     */
    protected $options = array();


    /**
     * @var SmartFocusTemplate $template
     */
    protected $template;

    /**
     * @var array<SmartFocusTemplate>
     */
    protected $templates = array();

    /**
     * @var array
     */
    protected $failures = array();

    /**
     * @var array
     */
    protected $messages = array();



    public function __construct(array $config = array())
    {
        $this->setOptions($config);
    }


    /**
     * Gets the template vars ready to fill
     * @return array
     */
    public function getDynVars()
    {
        return $this->template != null
            ? array_fill_keys($this->getTemplate()->getDyn(), null) :
            array();
    }

    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public abstract function send(array $messages);


    /**
     * Callback function used after sending message. First params
     * passed to the array is the mailer itself.
     *
     * @param $function
     */
    public function onSuccess($function)
    {
        if (is_callable($function)) {
            $args = func_get_args();
            array_shift($args);
            array_unshift($args, $this);
            call_user_func_array($function, $args);
        }
    }

    /**
     * Callback function on error. First params passed to the array
     * is the mailer itself.
     *
     * @param $function
     */
    public function onError($function)
    {
        if (is_callable($function)) {
            $args = func_get_args();
            array_shift($args);
            array_unshift($args, $this);
            call_user_func_array($function, $args);
        }
    }

    /**
     * Set the configuration options
     *
     * @param array $config
     * @return SmartFocusMailerInterface
     */
    public function setOptions(array $config)
    {
        $this->options = $config;

        if (isset($this->options['templates'])) {
            foreach ($this->options['templates'] as $name => $params) {
                $this->templates[$name] = new SmartFocusTemplate($name, $params);
            }
        }

        return $this;
    }

    /**
     * Set the template to use
     *
     * @param $template
     * @return $this|SmartFocusMailerInterface
     * @throws \Exception
     */
    public function setTemplate($template)
    {
        if ($template instanceof SmartFocusTemplate) {
            if (!in_array($template->getName(), array_keys($this->templates))) {
                $this->templates[$template->getName()] = $template;
            }
        }

        if (isset($this->templates[(string)$template])) {
            $this->template = $this->templates[(string)$template];
        } else {
            throw new \Exception(sprintf("The template %s is not configured or doesn't exist.", (string)$template));
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }


    /**
     * Return the actual template to use
     *
     * @return SmartFocusTemplate
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Return the templates availables
     * @return array<SmartFocusTemplate>
     */
    public function getTemplates()
    {
        return $this->templates;
    }

    /**
     * @return array
     */
    public function getFailures()
    {
        return $this->failures;
    }

    /**
     * @return array
     */
    public function getMessages()
    {
        return $this->messages;
    }

}