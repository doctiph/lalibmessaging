<?php
/**
 * Created by PhpStorm.
 * User: Ibrael Espinosa com-i.espinosa@lagardere-active.com
 * Date: 05/03/14
 * Time: 09:34
 */

namespace La\Lib\Messaging\Exception;


class NoSwiftMailerAvailableException extends \Exception
{

    protected $message = "The library Swiftmailer is not available or has not been loaded.";
} 