<?php
/**
 * Created by PhpStorm.
 * User: Ibrael Espinosa com-i.espinosa@lagardere-active.com
 * Date: 04/03/14
 * Time: 11:02
 */

namespace La\Lib\Messaging\Exception;


class NoCurlException extends \Exception
{
    protected $message = "There are no options defined for using curl";
}