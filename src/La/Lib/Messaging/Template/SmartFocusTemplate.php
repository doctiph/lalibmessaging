<?php
/**
 * Created by PhpStorm.
 * User: Ibrael Espinosa com-i.espinosa@lagardere-active.com
 * Date: 07/03/14
 * Time: 14:12
 */

namespace La\Lib\Messaging\Template;

/**
 * This class is the model of a smartfocus template object
 * Class SmartFocusTemplate
 *
 *
 * @package La\Lib\Messaging\Template
 */
class SmartFocusTemplate implements TemplateInterface
{

    protected $name;

    protected $random;

    protected $encrypt;

    protected $dyn = array();

    protected $content;

    public function __construct($name, array $params)
    {
        $this->setName($name);

        foreach ($params as $key => $value) {
            if (method_exists($this, 'set' . ucfirst($key))) {
                call_user_func_array(array($this, 'set' . ucfirst($key)), array($value));
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * The template name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    public function getUrlQuery()
    {
        return http_build_query(
            array(
                'random' => $this->getRandom(),
                'encrypt' => $this->getEncrypt()
            )
        );
    }

    /**
     * @return mixed $random
     */
    public function getRandom()
    {
        return $this->random;
    }

    /**
     * @param mixed $random
     */
    public function setRandom($random)
    {
        $this->random = $random;
    }

    /**
     * @return mixed $encrypt
     */
    public function getEncrypt()
    {
        return $this->encrypt;
    }

    /**
     * @param mixed $encrypt
     */
    public function setEncrypt($encrypt)
    {
        $this->encrypt = $encrypt;
    }

    /**
     * @return mixed
     */
    public function getParameters()
    {
        $params = array(
            'random' => $this->getRandom(),
            'encrypt' => $this->getEncrypt()
        );

        if ($this->hasContent()) {
            $params['content'] = $this->getContent();
        }

        return $params;
    }

    public function hasContent()
    {
        return !empty($this->content);
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getXML()
    {
        return sprintf("<random>%s</random><encrypt>%s</encrypt>", $this->getRandom(), $this->getEncrypt());
    }

    /**
     * @return array
     */
    public function getDyn()
    {
        return $this->dyn;
    }

    /**
     * @param mixed $dyn
     */
    public function setDyn(array $dyn)
    {
        $this->dyn = $dyn;
    }
}