<?php
/**
 * Created by PhpStorm.
 * User: Ibrael Espinosa com-i.espinosa@lagardere-active.com
 * Date: 07/03/14
 * Time: 14:14
 */

namespace La\Lib\Messaging\Template;


interface TemplateInterface {

    /**
     * The template name
     *
     * @return string
     */
    public function getName();
} 