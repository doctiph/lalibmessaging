<?php
/**
 * Created by PhpStorm.
 * User: Ibrael Espinosa com-i.espinosa@lagardere-active.com
 * Date: 13/03/14
 * Time: 11:35
 */

namespace La\Lib\Messaging\Message;


class SmartFocusMessage
{

    /**
     * @var array
     */
    public static $stypes = array('NOTHING', 'INSERT', 'UPDATE', 'INSERT_UPDATE');
    /**
     * @var string
     */
    protected $stype;
    /**
     * @var
     */
    protected $email;
    /**
     * @var array
     */
    protected $dyn = array();
    /**
     * @var string
     */
    protected $uidkey;
    /**
     * @var \DateTime
     */
    protected $senddate;
    protected $notificationId;

    function __construct(
        $email,
        array $dyn = array(),
        \DateTime $senddate = null,
        $stype = 'NOTHING',
        $uidkey = 'EMAIL'
    ) {

        $this->email = $email;

        $this->notificationId = uniqid();

        // if it is not established, we make an old address for sending inmmediatley
        $this->senddate = null !== $senddate ? $senddate : new \DateTime('now');

        if ($stype !== null) {
            if (!in_array($stype, self::$stypes)) {
                throw new \Exception(sprintf(
                    "Invalid stype for connecting. Availables are %s",
                    implode(', ', self::$stypes)
                ));
            } else {
                $this->stype = $stype;
            }
        } else {
            $this->stype = self::$stypes[0];
        }

        $this->uidkey = $uidkey ? : 'EMAIL';
        $this->dyn = $dyn;
    }

    public function __toString()
    {
        return $this->getEmail();
    }
    /**
     * @param $email
     * @param array $dyn
     * @param \DateTime $senddate
     * @param string $stype
     * @param string $uidkey
     * @return SmartFocusMessage
     */
    public static function newInstance(
        $email,
        array $dyn = array(),
        \DateTime $senddate = null,
        $stype = 'NOTHING',
        $uidkey = 'EMAIL'
    ) {
        return new SmartFocusMessage($email, $dyn, $senddate, $stype, $uidkey);
    }

    /**
     * @return string $url
     */
    public function getUrlQuery()
    {
        return http_build_query($this->getParameters());
    }

    public function getParameters()
    {
        return array(
            'email' => $this->getEmail(),
            'stype' => $this->getStype(),
            'senddate' => $this->getSenddate()->format('Y-m-d h:i:s'),
            'uidkey' => $this->getUidkey(),
            'dyn' => $this->getDynReady()
        );
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getStype()
    {
        return $this->stype;
    }

    /**
     * @param string $stype
     */
    public function setStype($stype)
    {
        $this->stype = $stype;
    }

    /**
     * @return \DateTime
     */
    public function getSenddate()
    {
        return $this->senddate;
    }

    /**
     * @param \DateTime $senddate
     * @return $this
     */
    public function setSenddate(\DateTime $senddate)
    {
        $this->senddate = $senddate;

        return $this;
    }

    /**
     * @return string
     */
    public function getUidkey()
    {
        return $this->uidkey;
    }

    /**
     * @param string $uidkey
     */
    public function setUidkey($uidkey)
    {
        $this->uidkey = $uidkey;
    }

    /**
     * Get the dyn ready to send
     *
     * @return string
     */
    public function getDynReady()
    {
        $values = array();
        foreach ($this->dyn as $key => $value) {
            $values[] = sprintf("%s:%s", $key, $value);
        }
        return implode('|', $values);
    }

    public function getXML()
    {
        $email = sprintf("<email>%s</email>", $this->getEmail());
        $synchrotype = sprintf("<synchrotype>%s</synchrotype>", $this->getStype());
        $senddate = sprintf(
            "<senddate>%s</senddate>",
            sprintf(
                "%sT%s",
                $this->getSenddate()->format('Y-m-d'),
                $this->getSenddate()->format('h:i:s')
            )
        );
        $uidkey = sprintf("<uidkey>%s</uidkey>", $this->getUidkey());
        $notificationId = sprintf("<notificationId>%s</notificationId>", $this->getNotificationId());

        $entries = "";
        foreach ($this->dyn as $key => $value) {
            $entries .= sprintf("<entry><key>%s</key><value>%s</value></entry>", $key, $value);
        }
        $dyn = sprintf("<dyn>%s</dyn>", $entries);

        return sprintf(
            "%s%s%s%s%s%s",
            $email,
            $senddate,
            $notificationId,
            $synchrotype,
            $uidkey,
            $dyn
        );
    }

    /**
     * @return string
     */
    public function getNotificationId()
    {
        return $this->notificationId;
    }

    /**
     * @return array
     */
    public function getDyn()
    {
        return $this->dyn;
    }

    /**
     * @param $dyn
     * @return $this
     */
    public function setDyn($dyn)
    {
        $this->dyn = $dyn;

        return $this;
    }
}