<?php
/**
 * Created by PhpStorm.
 * User: Ibrael Espinosa com-i.espinosa@lagardere-active.com
 * Date: 24/03/14
 * Time: 09:35
 */

namespace La\Lib\Messaging\Traits;


trait MailerApi
{

    //protected $failures = array();

    public function getBatchXml(array $messages = array())
    {
        $sendrequests = '';
        foreach ($messages as $msg) {
            $sendrequests .= $this->getSendRequestXml($msg);
        }

        return sprintf(
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
              <MultiSendRequest>%s</MultiSendRequest>",
            $sendrequests
        );
    }

    /**
     * @param $message
     * @return string
     */
    private function getSendRequestXml($message)
    {
        return sprintf("<sendrequest>%s%s</sendrequest>", $this->getTemplate()->getXml(), $message->getXml());
    }

    /**
     * Process the response after consulting the server!
     */
    private function extractErrorsFromResponse($response, &$failures = array())
    {
       if (isset($response['element']) && count($response['element']) > 0) {
            $response['element'] = (array)$response['element'];

            foreach ($response['element'] as $failure) {

                $failure = (array)$failure;

                if (isset($failure['@attributes']['responseStatus']) && $failure['@attributes']['responseStatus'] == 'failed') {
                    $this->failures[$failure['@attributes']['email']] = $failure['result'];
                }

                if (isset($failure['responseStatus']) && $failure['responseStatus'] == 'failed') {
                    $this->failures[$failure['email']] = $failure['result'];
                }
            }
        }
        $failures = array_merge($failures, $this->failures);
    }
} 