<?php
/**
 * Created by PhpStorm.
 * User: Ibrael Espinosa com-i.espinosa@lagardere-active.com
 * Date: 24/03/14
 * Time: 15:59
 */

namespace La\Lib\Messaging\Traits;


trait Connector {

    /**
     * @var string api_username
     */
    private $login;

    /**
     *
     * @var string api_password
     */
    private $pwd;

    /**
     * The manager key copied from SmartFocus.
     * @var string $key
     */
    private $key;

    /**
     * @var string api_token
     */
    private $token;

    /**
     * @var \SoapClient
     */
    private $soapClient;

    /**
     * Connection is open at datetime
     * @var \DateTime
     */
    private $openAt;

    function __construct($server, $login, $pwd, $key)
    {
        $this->login = $login;
        $this->pwd = $pwd;
        $this->key = $key;

        $url = sprintf("http://%s/apinmpreporting/services/NmpReportingService?wsdl", $server);
        $this->soapClient = new \SoapClient($url);
    }


    /**
     * Open the connection
     */
    public function openConnection()
    {
        try {
            $result = $this->soapClient->openApiConnection(
                array(
                    'login' => $this->login,
                    'pwd' => $this->pwd,
                    'key' => $this->key
                )
            );
            $this->openAt = new \DateTime("now");
            $this->token = $result->return;
        } catch (\SoapFault $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Close the connection to service
     */
    public function closeConnection()
    {
        try {
            $result = $this->soapClient->closeApiConnection(
                array(
                    'token' => $this->token,
                )
            );

            $this->openAt = null;
        } catch (\SoapFault $e) {
            throw new \Exception($e->getMessage());
        }
    }
} 