# Messaging #

Messaging is a library for sending emails over the smartfocus api. This library provides you with the tools for using email campaign plataforms
like smartfocus.com for sending you site emails.

# Basic Usage #

    // send emails with smartfocus api

    use La\Lib\Messaging\Message\SmartFocusMessage;
    use La\Lib\Messaging\Mailer\SmartFocusRestMailer;

    $config = array(
          'templates' => array(
              'lostpassword' => array(
                  'random' => '88020000A9373518',
                  'encrypt' => 'EdX7CqkmmqjQ8SA9MKJPUYHfIEx4HK3D-knUeK41XcHRKwE',
                  'dyn' => array('firstname', 'lastname')
              ),
              'testmessage' => array(
                  'random' => '54E2200800007CB5',
                  'encrypt' => 'EdX7CqkmmqhM8SA9MKJPUYHSLDl4Hq3D8jjde6kxK7LcK4k',
                  'dyn' => array('firstname', 'sender', 'lastname')
              )
          )
      );

    $mailer = new SmartFocusRestMailer($config);

    // or
    // $mailer = new SmartFocusSoapMailer($config);

    // or
    // $mailer = new SmartFocusFGetMailer($config);

The configuration is quite simple:
**templates** is the list of templates created into smartfocus. **random** is the number assigned to template
while **encrypt** is the api key for this template. **dyn** are the **EMV DYN** variables names defined for this template into smartfocus.

# Clients #

Several clients are proposed in order to get full compatibility. In the example the rest api client is used, but you can change it for
the SmartFocusSoapMailer (soap) or the SmartFocusFGetMailer (fgets).

You can create also your own client implementing the **SmartFocusMailerInterface** or extending the **Mailer** class provided.

After configurate the api client, lets create the message to send.

    $messages = array(
        SmartFocusMessage::newInstance('com-i.espinosa@lagardere-active.com'),
        SmartFocusMessage::newInstance('ibrael.avalanch@gmail.com'),
        SmartFocusMessage::newInstance('ibrael@mailHazard.com', null, SmartFocusMessage::$stypes[0], null, array(
                'firstname' => 'Ibrael', 'lastname' => 'Espinosa'
            )),
        SmartFocusMessage::newInstance('PierreYves.Chansigaud@lagardere-active.com'),
    );
# Setting the template to use #

For specifying the template to use, you will call the method setTemplate($name) as follow:

    $mailer->setTemplate('testmessage');

# Sending the messages #

Sending the emails is as simple as calling the method send from the client! The **messages** parameter must be an array of
[SmartFocusMessage](http://webgit.in.ladtech.fr/?p=LaLibMessaging.git;a=blob;f=src/La/Lib/Messaging/Message/SmartFocusMessage.php;h=765ce59ef71b6d70ffb9b0cde0f5534c550eede7;hb=refs/heads/develop) objects.


    try{
        $sent  = $mailer->send($messages);

        echo sprintf("Sent %s emails", $sent);

    }catch (\Exception $e)
    {
        print_r($e->getMessage());
        print_r($e->getTraceAsString());
    }


# Batch emails #

The library is smart enough to know when to send emails in batch mode or in single mode.
If messages array entered in the **send()** method contains more than 1 SmartFocusMessage object,
then it will trigger the batch mode.

# Callback error and success calls #

Here you have un example of how using the callbacks functions.

    $mailer->onSuccess(
        function (Mailer $mailer, Curl $instance, Logger $logger // other params){
            $response = (array)simplexml_load_string($instance->response);

            if ($this->logger != null) {
                $this->logger->info(
                    sprintf(
                        "Send email to %s with template %s. Server responded: %s",
                        $response['@attributes']['email'],
                        $mailer->getTemplate(),
                        $response['@attributes']['responseStatus'],
                        $response['result']
                    )
                );
            }

            $dispatcher->dispatch(EmailEvents::EMAIL_SENT_WITH_SUCCESS, new EmailEvent($mailer, $mailer->getMessages(), $mailer->getTemplate(), $response));

            return true;
        },
        $logger,
        $dispatcher

        // other params
    );


# Automatic configuration #

Well, its time to configure templates, but you have already 30 differents templats into smartfocus and configurate them will took you all morning.
The RequestConfiguration class come make the work for you!

Lets see:

    use La\Lib\Messaging\Configuration\RequestConfiguration;

    $requestConfig = new RequestConfiguration('my_api_server.com', 'my_username', 'my_pass', 'my_api_key');

    $requestConfig->openConnection();

    // availables types: TRANSACTIONAL, SOCIALNETWORK
    $templates = $requestConfig->getConfiguredTemplates('TRANSACTIONAL');

    $requestConfig->closeConnection();

There you will get all templates configured in array format, included the dyn vars.


happy coding!





