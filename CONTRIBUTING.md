Contributing
============

If you've written a new client, adapted Messaging in some way, or fixed a bug, your contribution is welcome!

Before proposing a pull request, check the following:

* Your code should follow the [PSR-2 coding standard](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-2-coding-style-guide.md) (and use [php-cs-fixer](https://github.com/fabpot/PHP-CS-Fixer) to fix inconsistencies).
* Unit tests should still pass after your patch
* As much as possible, add unit tests for your code
* Avoid changing existing sets of data. Some developers use Faker with seeding for unit tests ; changing the data makes their tests fail.
* Speed is important in all Messaging usages. Make sure your code is optimized without consuming too much memory or CPU.
* If you commit a new feature, be prepared to help maintaining it. Watch the project on git or jira repository, and please comment on issues or PRs regarding the feature you contributed.

Thank you for your contribution! Faker wouldn't be so great without you.