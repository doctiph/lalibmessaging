<?php
/**
 * Created by PhpStorm.
 * User: Ibrael Espinosa com-i.espinosa@lagardere-active.com
 * Date: 24/03/14
 * Time: 10:33
 */

namespace La\Lib\Messaging\Mailer;

use La\Lib\Messaging\Message\SmartFocusMessage;

class SmartFocusFGetMailerTest extends \PHPUnit_Framework_TestCase
{

    protected $smfget;

    /**
     * setting up the object
     */
    public function setUp()
    {
        $config = array(
            'url' => "api.notificationmessaging.com",
            'templates' => array(
                'lostpassword' => array(
                    'random' => '88020000A9373518',
                    'encrypt' => 'EdX7CqkmmqjQ8SA9MKJPUYHfIEx4HK3D-knUeK41XcHRKwE',
                    'dyn' => array('firstname', 'lastname')
                ),
                'testmessage' => array(
                    'random' => '54E2200800007CB5',
                    'encrypt' => 'EdX7CqkmmqhM8SA9MKJPUYHSLDl4Hq3D8jjde6kxK7LcK4k',
                    'dyn' => array('firstname', 'sender', 'lastname')
                ),
                'gulli_test' => array(
                    'random' => 'B58F6A88020001EB',
                    'encrypt' => 'EdX7CqkmmquO8SA9MKJPXAylLUQMGtzL8jjfe6k2WbWrKGc',
                    'dyn' => array()
                )
            )
        );

        $this->smfget = new SmartFocusFGetMailer($config);
    }

    /**
     * @test
     * @requires setUp
     */
    public function testSend()
    {

        $messages = array();
        $failures = array();
        $sent = 0;

        for ($i = 0; $i < 10; $i++) {
            $messages[] = SmartFocusMessage::newInstance('ibrael@mailHazard.com', array());
        }


        $messages = array_merge(
            $messages,
            array(
                SmartFocusMessage::newInstance('com-i.espinosa@lagardere-active.com'),
                SmartFocusMessage::newInstance('ibrael1.avalanch'),
                SmartFocusMessage::newInstance('ibrael2.avalanch'),
                SmartFocusMessage::newInstance('ibrael3.avalanch'),
                SmartFocusMessage::newInstance(
                    'ibrael@mailHazard.com',
                    array(
                        'firstname' => 'Ibrael',
                        'lastname' => 'Espinosa'
                    ),
                    new \DateTime('now')
                ),
                SmartFocusMessage::newInstance('PierreYves.Chansigaud@lagardere-active.com'),
            )
        );


        $this->smfget->setTemplate('gulli_test');
        $sent = $this->smfget->send($messages, $failures);


        $this->assertSelectCount(
            'sendrequest',
            count($messages),
            $this->smfget->getBatchXml($messages),
            'Invalid number of sendrequest elements',
            false
        );
        $this->assertEquals(13, $sent, "Sending 103 emails to api");
        $this->assertEquals(3, count($failures), "Receiving failed emails");
    }

    /**
     * Testing the getBatchXml() method
     * @test
     */
    public function testGetBatchXml()
    {
        $messages = array(
            SmartFocusMessage::newInstance('com-i.espinosa@lagardere-active.com'),
            SmartFocusMessage::newInstance('ibrael1.avalanch'),
            SmartFocusMessage::newInstance('ibrael2.avalanch'),
            SmartFocusMessage::newInstance('ibrael3.avalanch'),
            SmartFocusMessage::newInstance(
                'ibrael@mailHazard.com',
                array(
                    'firstname' => 'Ibrael',
                    'lastname' => 'Espinosa'
                ),
                new \DateTime('now')
            ),
            SmartFocusMessage::newInstance('PierreYves.Chansigaud@lagardere-active.com'),
        );


        $this->smfget->setTemplate('gulli_test');

        $this->assertSelectCount(
            'sendrequest',
            count($messages),
            $this->smfget->getBatchXml($messages),
            'Invalid number of sendrequest elements',
            false
        );

        $this->assertSelectEquals(
            'sendrequest > dyn > entry > value',
            $messages[4]->getDyn()['lastname'],
            1,
            $this->smfget->getBatchXml($messages),
            'Invalid content of lastName element',
            false
        );
    }

}
 