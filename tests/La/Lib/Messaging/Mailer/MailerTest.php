<?php
/**
 * Created by PhpStorm.
 * User: Ibrael Espinosa com-i.espinosa@lagardere-active.com
 * Date: 24/03/14
 * Time: 10:33
 */

namespace La\Lib\Messaging\Mailer;
use La\Lib\Messaging\Mailer\SmartFocusRestMailer;
use La\Lib\Messaging\Message\SmartFocusMessage;
use La\Lib\Messaging\Template\SmartFocusTemplate;

class MailerTest extends \PHPUnit_Framework_TestCase {

    protected $smrest;
    /**
     * setting up the object
     */
    public function setUp()
    {
        $this->smrest = new SmartFocusRestMailer(array(
            'templates' => array(
                'lostpassword' => array(
                    'random' => '88020000A9373518',
                    'encrypt' => 'EdX7CqkmmqjQ8SA9MKJPUYHfIEx4HK3D-knUeK41XcHRKwE',
                    'dyn' => array('firstname', 'lastname')
                ),
                'testmessage' => array(
                    'random' => '54E2200800007CB5',
                    'encrypt' => 'EdX7CqkmmqhM8SA9MKJPUYHSLDl4Hq3D8jjde6kxK7LcK4k',
                    'dyn' => array('firstname', 'sender', 'lastname')
                )
            )
        ));
    }

    /**
     * @test
     * @requires setUp
     */
    public function testSetTemplate()
    {
        $this->smrest->setTemplate('lostpassword');

        $this->assertEquals('lostpassword', $this->smrest->getTemplate()->getName());
    }
}
 