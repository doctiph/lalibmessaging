<?php
/**
 * Created by PhpStorm.
 * User: Ibrael Espinosa com-i.espinosa@lagardere-active.com
 * Date: 24/03/14
 * Time: 12:55
 */

namespace La\Lib\Messaging\Traits;
use La\Lib\Messaging\Traits\MailerApi;

class MailerApiTest extends \PHPUnit_Framework_TestCase {

    use MailerApi;

    public function testProcessResponse()
    {
        $response = array(
            'element' => array(
                array(
                    '@attributes' => array(
                        'email' => 'pepe.lagardere-act.com',
                        'responseStatus' => 'failed'
                    ),
                    'result' => 'Email malformed'
                ),
                array(
                    '@attributes' => array(
                        'email' => 'alberto.lagardere-act.com',
                        'responseStatus' => 'failed'
                    ),
                    'result' => 'Email malformed'
                )
            )
        );

        $failures = array();
        $this->extractErrorsFromResponse($response, $failures);

        $this->assertEquals(2, count($failures), "Testing failures extraction from response");
    }

}
 