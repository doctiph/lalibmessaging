<?php
/**
 * Created by PhpStorm.
 * User: Ibrael Espinosa com-i.espinosa@lagardere-active.com
 * Date: 04/03/14
 * Time: 11:20
 */
//namespace tests;

require_once __DIR__ . "/../src/autoload.php";


use La\Lib\Messaging\Message\SmartFocusMessage;

$sm = new La\Lib\Messaging\Mailer\SmartFocusRestMailer(array(
    'templates' => array(
        'lostpassword' => array(
            'random' => '88020000A9373518',
            'encrypt' => 'EdX7CqkmmqjQ8SA9MKJPUYHfIEx4HK3D-knUeK41XcHRKwE',
            'dyn' => array('firstname', 'lastname')
        ),
        'testmessage' => array(
            'random' => '54E2200800007CB5',
            'encrypt' => 'EdX7CqkmmqhM8SA9MKJPUYHSLDl4Hq3D8jjde6kxK7LcK4k',
            'dyn' => array('firstname', 'sender', 'lastname')
        )
    )
));
try {

    $sm->setTemplate('testmessage');
    $messages = array();

    for ($i = 0; $i < 100; $i++) {
        $messages[] = SmartFocusMessage::newInstance('ibrael@mailHazard.com', array());
    }


$messages = array_merge($messages, array(
    SmartFocusMessage::newInstance('com-i.espinosa@lagardere-active.com'),
    SmartFocusMessage::newInstance('ibrael1.avalanch'),
    SmartFocusMessage::newInstance('ibrael2.avalanch'),
    SmartFocusMessage::newInstance('ibrael3.avalanch'),
    SmartFocusMessage::newInstance(
        'ibrael@mailHazard.com',
        array(
            'firstname' => 'Ibrael',
            'lastname' => 'Espinosa'
        ),
        new \DateTime('now')
    ),
    SmartFocusMessage::newInstance('PierreYves.Chansigaud@lagardere-active.com'),
));


    $sent = 0;
    $failures = array();
    $sm->setTemplate('lostpassword');

    $doc = new DOMDocument('1.0', 'utf-8');
    $doc->loadXML($sm->getBatchXml($messages));
    $doc->formatOutput = true;
    $doc->save('test.xml');



    $start = microtime();
    $sent += $sm->send($messages, $failures);
    $end = microtime();
    echo sprintf(
        "Sent %s emails, %d failures. request took %f seconds",
        $sent,
        count($sm->getFailures()),
        $end - $start
    );

    print_r($failures);

} catch (\Exception $e) {
    print_r($e->getMessage());
    print_r($e->getTraceAsString());
}


