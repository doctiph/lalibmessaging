<?php
/**
 * Created by PhpStorm.
 * User: Ibrael Espinosa com-i.espinosa@lagardere-active.com
 * Date: 18/03/14
 * Time: 15:00
 */


require_once __DIR__ . "/../src/autoload.php";


use La\Lib\Messaging\Mailer\SmartFocusSoapMailer;
use La\Lib\Messaging\Message\SmartFocusMessage;

$config = array(
    'url' => "http://api.notificationmessaging.com/",
    'templates' => array(
        'lostpassword' => array(
            'random' => '88020000A9373518',
            'encrypt' => 'EdX7CqkmmqjQ8SA9MKJPUYHfIEx4HK3D-knUeK41XcHRKwE',
            'dyn' => array('firstname', 'lastname')
        ),
        'testmessage' => array(
            'random' => '54E2200800007CB5',
            'encrypt' => 'EdX7CqkmmqhM8SA9MKJPUYHSLDl4Hq3D8jjde6kxK7LcK4k',
            'dyn' => array('firstname', 'sender', 'lastname')
        )
    )
);


$messages = array();

for($i = 0; $i < 5; $i++)
    $messages[] = SmartFocusMessage::newInstance(sprintf("mailHazard.com", $i), array());

$messages[] = SmartFocusMessage::newInstance(sprintf("mailHazard.com"), array());
$failures = array();


$mailer = new SmartFocusSoapMailer($config);
$mailer->setTemplate('lostpassword');
$start = microtime();
$sent =$mailer->send($messages, $failures);
$end = microtime();

print sprintf("%d emails sent, %d fails. Request took %s miliseconds ", $sent, count($failures), $end - $start);

if(count($failures))
    print_r($failures);