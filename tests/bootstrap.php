<?php
/**
 * Created by PhpStorm.
 * User: Ibrael Espinosa com-i.espinosa@lagardere-active.com
 * Date: 05/03/14
 * Time: 13:00
 */
namespace La\Lib\Messaging\Test;

$namespaces = array(
    'La\\Lib\\Messaging\\Test' => "tests/",
    'La\\Lib\\' => "src/",
);

spl_autoload_register(
    function ($className) use ($namespaces) {

        if (class_exists($className)) {
            return true;
        }

        $className = ltrim($className, '\\');
        $fileName = '';
        $namespace = '';
        if ($lastNsPos = strripos($className, '\\')) {
            $namespace = substr($className, 0, $lastNsPos);
            $className = substr($className, $lastNsPos + 1);
            $fileName = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
        }
        $fileName = __DIR__ . DIRECTORY_SEPARATOR . $fileName . $className . '.php';


        foreach ($namespaces as $namespace => $dir) {
            $namespace = str_replace('\\', DIRECTORY_SEPARATOR, $namespace);

            print_r(sprintf("%s\n\n", strpos($fileName, $namespace)));

            if (strpos($fileName, $namespace) == 0) {
                print_r(sprintf("loading %s\n\n", $fileName));

                $fileName = preg_replace($namespace, $dir, $fileName);
            }
        }

        if (file_exists($fileName)) {
            require $fileName;
            return true;
        }

        return false;
    }
);
